# Plant UML examples

Browse the [PlantUML web site](https://plantuml.com/) for a full documentation

## Class Diagram

![example](example.svg)
