# Modèle de données du SAR

## Classes communes

![references](img/references.svg)


## Typologie d'ouvrages

![coastal-structure](img/coastal-structure.svg)

## Transcodage

![transcribing](img/transcribing.svg)
