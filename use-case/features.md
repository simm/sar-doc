# Fonctionnalités

## **Consultation** de référentiels

- Interlocuteurs
- Typologies d'ouvrages côtiers
- Référentiel taxonimique

---
## **Administration** des référentiels

- Gestion des typologies d'ouvrages côtiers
