[Retour au sommaire de l'aide en ligne](../index.md)

# Recherche Interlocuteurs


- Recherche par chaine de caractères :
  - Recherche OU (par défaut) si espace entre les mots. Par exemple `ofb gard` (sans quote) signifie `ofb` OU `gard`
  - Recherche ET : si + entre les mots. Par exemple `ofb+gard` signifie ofb ET gard
  - Recherche avec asterix : si * entre les mots. Par exemple `ofb*gard` signifie ofb(...)gard
  - Recherche par phrase, avec une double quote : Par exemple "ofb gard"
