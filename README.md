# SAR Documentation

## Table of content

- [Project management](project/index.md) (Meeting reports, planning, specifications)

- [Data model](model/index.md) (Class diagram)

- [Functional use case (FR)](use-case/index.md)

- [User manual](user-manual/index.md)

## License

All documents and source code are under [License GPL v3](./LICENSE), except user manual that use a CC-by-SA license.

## Links

- SAR technical web site: http://doc.e-is.pro/sar/
