# Spécifications

===
## Consultation des typologies d'ouvrage

- Documentation : 
  - [Besoins outil de diffusion Typologie des ouvrages (v3)](project/doc/sar-doc-22-001-Besoins_outil_de_diffusion_Typologie_des_ouvrages-v3.docx)
- Fonctionnalités
  - Vue arbre
  - Recherche avec affichage tableau 
  - Export (CSV, RDF, etc.)
  - Page de détail

---
## Consultation des typologies d'ouvrage
- Vue arbre :
  * Titre ? Lien d'aide ?
    - Identique a la vue recherche (les deux vue partagent le même composant) [Fait].
  * Identifiant cliquable ? (vers la page détail) [Fait]
  * Badge avec l'état ? (cf [geoscience.fr/ncl](https://data.geoscience.fr/ncl/_apt))
    * Voire uniquement les états actif par défaut. Avec possibilité de voir les gelés. [en cours]
  * Inclure le nom type des items avec leur nom [Fait].
  * Faut-il rendre visible la description ?
    + Oui (confirmé) [Fait].
  * Bouton "tout déplier" / "tout replier" ? [Fait]
  * Bouton basculer recherche tableau ? [Fait]
  * Bouton téléchargement ? [Fait]
  * Accéder au détaille via un bouton dédié [Fait]
  * Vue mobile :
    - Un tap sur un item doit afficher les enfants [Fait]

---
## Consultation des typologies d'ouvrage

- Recherche tableau :
  * Affichage des résultats : [Fait]
    + Ne pas afficher les codes et catégorie, a la place fournir le type et le parent [Fait].
  * Critères de filtre : [Fait]
    * "code" : Recherche dans identifiant SAR ET codes alternatifs [Fait]?
    * "mots clefs" : via la recherche générale [Fait]
    * "catégorie" : liste déroulante avec les niveaux 1 (racine) [Fait]
    * "classe" : liste déroulante avec les niveaux 2 [Fait]
    * Filtres classe et catégories [Fait].
  * Téléchargement de toutes les données [Fait]
    - deux format possible :
      + Tabular : basé le les colonnes de la table [Fait]
      + Web sémantique : basé sur une extraction directe des données du end point
        sparql [Fait]
    - ~~Dans tout les cas xml, json, csv.~~
  * Bouton switch vers vue arbre [Fait]

---
## Consultation des typologies d'ouvrage
- Page détails : 
  * Possibilité d'ouvrir l'URI directement ?
  * Rendu via :
    + l'App (un seul docker avec config nginx)
    + ou via une page générée par le Pod
  * Téléchargement de toutes les données [Fait]
    + Deux format possibles :
      - Tabular : basé le les colones de la table (mais uniquement avec les
        données de l'item courant) [Fait].
      - Web semantic : basé sur une extraction directe des données du end point
        Sparql [Fait].
    - ~~Dans tout les cas xml, json, csv.~~
  * Bouton switch vers vue recherche [Fait]
  * Ne pas afficher les codes et catégorie, a la place fournir le type et le parent (parent cliquable) [Fait]
  * Faire figurer le niveau de l'item [Fait]
  * Possibilité d'éditer les données en mode app ?
  * Présentation des données similaire a Interlocuteur [Fait]


===
## Consultation des interlocuteurs

- [Tickets](https://gitlab.ifremer.fr/groups/simm/-/issues) à traiter
  * Export complet [Fait]
  * Bug date sous FireFox [Fait]
  * Autre ?
  
===
## Consultation du référentiel taxinomique
- Documentation ?
- Spécification : à rédiger (via tickets du PoC ?)

===
## Consultation des ports

- Documentation :
  - [Besoins outil de diffusion Typologie des ports (v2)](project/doc/sar-doc-22-002-Besoins_outil_de_diffusion_Typologie_des_ports-v2.docx)

