# Suivi du contrat

## Contrat 2022-2023

> Bon de commande n°4500078561 reçu le 24/10/22

---
## Consommé EIS

| Description                     | Intervenant | Date     | Conso (j) | RAF (j) |
|:--------------------------------|:------------|:---------|----------:|--------:|
| **Suivi de projet** :           |             |          |           |         |
| - Réunions + suivi              | BLA, MDE    | 24/03/23 |       1.5 |         |
| - Présentation technique SAR    | BLA         | 20/10/22 |      0.75 |         |
| **Installation** :              |             |          |           |         |
| - Installation poste de travail | MDE         | 19/10/22 |       1.5 |         |
| **Mise à jour du socle** :      |             |          |           |         |
| - Pod (Gestion téléchargement)  | BLA, MDE    | 08/02/23 |         2 |       0 |
| - Pod (Spring+Hibernate)        | BLA, LPT    | 08/02/23 |      5.25 |       5 |
| - Pod (Réduction code)          | BLA         |          |      0.25 |     3.5 |
| - App (Angular 14)              | MDE         | 08/02/23 |      2.25 |       0 |
| - App (Landing page)            | MDE         | 08/02/23 |      0.25 |       1 |
| - Mise a jour CI                | MDE         | 24/03/23 |       0.5 |       0 |
| **Développements** :            |             |          |           |         |
| - CST - Vue arbre               | MDE         | 22/02/23 |       6.5 |       0 |
| - CST - Recherche tableau       | MDE         | 24/03/23 |      16.5 |       0 |
| - CST - Export                  | MDE         | 22/02/23 |       8.5 |       0 |
| - CST - Page détail             | MDE         | 22/02/23 |      4.75 |       0 |
|                                 |             |          |           |         |
| **Total :** (24/03/2023)        |             |          |  **49 j** |         |
