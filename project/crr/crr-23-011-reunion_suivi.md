## Réunion de suivi

### 24/03/2023 à 11h00

Prochaine réunion de suivi : Non planifié

> Présents :
>
> - Glenn JUDEAU (Ifremer)
> - Christian BONNET (Ifremer)
> - Benoit LAVENIER (EIS)
> - Maxime DEMAREST (EIS)

---
# Organisation projet

---
# RAF / Mise en production

1. Compléter la fiche de mise en production (action Ifremer);
2. Ecrire le fichier de configuration pour le production (action Ifremer);
    * Relecture par EIS
---
# Prochains chantiers

1. Finalisation la migration du socle (vers Angular 14, Java 17)
    * Simplifier le fichier de configuration en préproduction (y compris le profile `default`);
2. Nouvelle URL pour accéder aux "landings pages" via les URI
    * Conséquence pour le déploiement : nouvelle URL pour l'APP (en id.milieumarinfrance.fr)

---
# Avancement

- Release 1.1.2 app et pod prêtres pour la prod.
    - Update gitlab-ci
    - Ordre de tris éèê naturel sur coastal structure type table.
 
> Consommé : [49j au 24/03/23](project/sui/sui-22-001-suivi_consomme_eis.md)

---
# Planning

![Gantt](project/images/planning-suivi.svg)t

---
# Actions

- Faire la release de la 1.1.3 incluant l'issue [#53](https://gitlab.ifremer.fr/simm/sar-app/-/issues/53)

---
# Question
 