
## Réunion de suivi

### 23/12/2022


Prochaine réunion de suivi : 12/01/2023 à 14h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : réalisée
  * App : en cours
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Recherche
    - Remplacer tab par bouton toggle vue table/arbre.
  * Table :
    - Filtres OK.
    - Export avec filtres.
  * Arbre :
    - Chemin des éléments de l'arbre visible.
  * Page :
    - Commencement de la fonctionnalité noter cette page.
    - Retours recherche.
  * Divers
    - Amélioration design.


> Consommé : [XXj au 23/12/22](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- EIS (23/12/2022) :
  - Mise en place du models de données réel sur le POD.
  - Basculement des donnés du moc vers les données réels.