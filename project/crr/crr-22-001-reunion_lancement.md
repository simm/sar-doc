## Réunion de lancement
### 07/10/2022

> Présents :
>
> - Antoine HUGUET (IFREMER)
> - Christian BONNET (IFREMER)
> - Glenn JUDEAU (IFREMER)
> - Benoit LAVENIER (EIS)
> - Maxime DEMAREST (EIS)

Prochaine : 1er réunion de suivi : 28/10/2022 à 11h

---

# Sommaire

- [Organisation de la prestation](#organisation-de-la-prestation)
  * [Équipe EIS](#quipes-eis)
  * [Équipe Ifremer](#quipe-ifremer)
  * [Resources & Documentation](#resources--documentation)
  * [Test et validation](#tests-et-validation)
- [Périmètre](#primtre)
- [Planning](#planning-12)
- [Actions](#actions)

---
# Organisation de la prestation 
## Équipes EIS

![Équipe EIS](project/images/team-eis.png)

---
## Équipe Ifremer

> - Antoine HUGUET - Responsable MOA
> - Glenn JUDEAU - Responsable MOE (1 j/semaine)
> - Christian BONNET - Expertise fonctionnelle et technique ponctuelle
> - Equipe SAR (spécifications et validation) :
    >   * Armelle ROUYER (Sismer)
>   * Clémence COTTEN (Sismer)
>   * Steven PIEL (OFB)

---
## Resources & Documentation

- Sources : [gitlab.ifremer.fr/simm/](https://gitlab.ifremer.fr/simm/)
- Documentation : [gitlab.ifremer.fr/simm/sar-doc/](https://gitlab.ifremer.fr/simm/sar-doc/-/tree/master)
  * Compte-rendus de réunion (format MarkDown)
  * UML (plantUML)
- Chat (Mattermost): Salon ["SIMM - SAR Outils"](https://gitlabchat.ifremer.fr/simm-ifremer/channels/simm---sar-outils)

> Compte-rendu :
> - Christian demande à pouvoir suivre les modifications des CRR, par exemple
    >  via une surbrillance. EIS va étudier la faisabilité

---
## Tests et validation

- Livraison : Image Docker, construite par Gitlab CI
- Déploiement:
  * Tests : visi-common-docker
  * Validation (équipe du SAR) : isi-val

---
# Périmètre

Priorités 2022
> - Consolidation du socle technique
> - Consultation des typologies d'ouvrages

Priorités 2023
> - Consultation du référentiel taxinomique : soit début 2023, soit printemps 2023
> - Consultation des paramètres
> - Consultation des ports ?

**Fonctions d'administration** en stand-by

---
# Planning (1/2)
## Réponse EIS

![Gantt](project/images/planning-ao.svg)

---
# Planning (2/2)
## Mise à jour

- Démarrage des développements : 17/10/2022

![Gantt](project/images/planning-lancement.svg)


---
# Actions
- 17/10/2022 - EIS - Envoi présentation technique
- 20/10/2022 - Visio - Présentation technique (partie consultation)
  SparQL, Pod (PostgreSQL), App (Angular)

  10 min présentation

---
## Liens

- Anomalies et évolutions (GitLab) :<br/>
https://gitlab.ifremer.fr/simm/sar-app/-/issues
- Documentation, compte-rendus réunion :<br/>
https://gitlab.ifremer.fr/simm/sar-doc/
- Chat (Mattermost): Salon ["SIMM - SAR Outils"](https://gitlabchat.ifremer.fr/simm-ifremer/channels/simm---sar-outils)
