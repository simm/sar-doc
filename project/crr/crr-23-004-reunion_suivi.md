
## Réunion de suivi

### 31/01/2023


Prochaine réunion de suivi : 08/02/2023 à 17h30

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : en cours
  * App : en cours
- Pod :
- Typologie des ouvrages littoraux (CST) : **en cours**


> Consommé : [35,5j au 12/01/23](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- EIS (23/12/2022) :