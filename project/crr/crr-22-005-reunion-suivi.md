## Réunion de suivi

### 02/12/2022


Prochaine réunion de suivi : 09/12/2022 à 15h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : réalisée
  * App : en cours
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Pas d'avancement sur le visuel.
  * Retours sur la maquette.

> Consommé : [17j au 02/12/22](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- Glenn (09/12/22) :
  * Fournir les retours sur la maquette via les issues du [GitLab](https://gitlab.ifremer.fr/simm/sar-app/-/issues)
- EIS (09/12/2022) :
  * Typologie des ouvrages littoraux (CST) : **en cours**
    + Page de recherche :
      - Intégrer le composant tree view dans la vue recherche.
        * Switcher de l'un a l'autre via un bouton toggle.
        * Permettre de charger l'un ou l'autre via un paramètre de l'url (choix de l'affichage au clic sur un lien avec capture d'écran depuis le site).
      - Remplacer les colonnes code/catégorie, par colonnes niveau/parent.
      - Activer le filtre sur la Classe et la Catégorie.
      - Colonne status.
      - Téléchargement non fonctionnel (à fixer).
      - Dates
    + Page de détail :
      - Bouton retours recherche/arbre (sur vue embded).
      - Retirer bouton téléchargement du bas quant mode app.
      - Ajouter téléchargement type table (similaire à l'extraction recherche, mais seulement avec les données de l'item courant).
      - Export xml,json et csv pour vu type table et web sémantique.
      - Ajouter date mise à jour même si vide (text inconnu si vide).
      - Affichage autre que vue "key/value" (essayer le titre sur sa propre ligne).
    + Vue en arbre :
      - Ajouter badge coloré en fonction du status.
      - Ne pas présenter le type sous forme de badge (peut être une annotation du titre).
      - Ajouter bouton options avec item pour afficher/masquer élément avec statue gelé.
      - ~~Déplier replier au clic sur la ligne (adapter au écran tactile)~~
        * Non, d'après le retours la préférence va au clic sur le titre.
        * Si plateforme mobile, n’afficher de base que le nom de l'item et voir à la fois le détail et les enfants au tap sur l'item. La vue détaillée est accessible via un bouton dans le détaille.