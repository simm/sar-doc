## Réunion de suivi

### 18/11/2022


Prochaine réunion de suivi : 25/11/2022 à 11h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Benoit LAVENIER (EIS)
> - Antoine HUGUET (IFREMER)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : réalisée
  * App : en cours
- Socle: Fix du bug #28 (format date)
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Page de recherche :
    - ajout champs recherche catégorie et class
    - export CSV, JSON, XML
    - export de toutes les données pas seulement la page courrante
  * Page de détail :
    - présentation basique des données mise en page

> Consommé : [12j au 04/11/22](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- Glenn (25/11/22) : Demande SAR concernant le page landing (graphique bandeau vue embarquée)
- EIS (25/11/2022) :
  - Maquettage de la vue arbre (description dépliable, etc.)
  - Maquettage page de détails
    - Intégration site (vue embarqué)
      - bandeau SAR
      - faux fils d'Ariane (retour recherche, accueil, etc...)
  - Exports :
    - Sauf CSV, autres export web sémantique
    - Éventuellement, sous menus export, tabular/web sémantique
