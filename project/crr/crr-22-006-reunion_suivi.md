## Réunion de suivi

### 09/12/2022


Prochaine réunion de suivi : 15/12/2022 à 16h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : réalisée
  * App : en cours
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Fusion vue recherche et arbre.
  * Ajout boutons status (sur table, arbre et consultation).
  * Amélioration diverse sur le design.


> Consommé : [20j au 02/12/22](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- EIS (09/12/2022) :
  - Typologie des ouvrages littoraux (CST) :
    * Voir les issues sur le Gitlab : <https://gitlab.ifremer.fr/simm/sar-app/-/issues>
    * Placer les tags de niveau de structure côtière avant les titres.
    * Dans la table, les nom des typology doivent être clickable.
    * Nom de fichier par défaut dans les exports.