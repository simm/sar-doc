## Réunion de suivi

### 25/11/2022


Prochaine réunion de suivi : 02/12/2022 à 11h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Benoit LAVENIER (EIS)
> - Antoine HUGUET (IFREMER)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : réalisée
  * App : en cours
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Donnée complètes (basé sur une extraction du CSV fournis en attendant
    que le modèle du end point sparql soit prêt).
  * Page de recherche :
    + Nom des catégorie et classe dans les filtres de recherche.
    + Affichage des catégories et classes (code uniquement) dans les colonnes
      du tableau (lien cliquable, renvois vers la description de l'item).
    + Menu téléchargement avec extraction web sémantique JSON et XML.
  * Page de détail :
    + Vue embded.
    + Téléchargement de l'item en web sémantique.
    + Ajout ligne catégorie et classe (uniquement si l'item en possède).
  * Vue en arbre :
    + Création de la maquette.

> Consommé : [15j au 25/11/22](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- Glenn (02/12/22) :
  * Déployer la maquette.
  * Fournir les premiers retours sur la maquette.
- EIS (02/12/2022) :
  * Typologie des ouvrages littoraux (CST) : **en cours**
    + Page de recherche :
      - Ajouter colonne avec le niveau de l'item (Catégorie, Classe, Type).
      - Activer le filtre sur la Classe et la Catégorie.
      - Bouton toogle entre vue recherche et vue arbre (fusionner les deux vues
        dans le même composant).
      - Afficher le statue.
      - Bouton de téléchargement des données de la ligne.
      - Afficher le nom des classe et des catégories (rendre le tableau
        scrollable horizontalement si nécessaire).
    + Page de détail :
      - Bouton retours recherche/arbre (sur vue embded).
      - Retirer bouton téléchargement sur du bas quant mode app.
      - Ajouter téléchargement tabular (similaire a l'extraction recherche mais seulement avec les données de l'item courant).
      - Export xml,json et csv pour tabular et web sémantique.
    + Vue en arbre :
      - Ajouter badge coloré en fonction du statue.
      - Colorer le type.
      - Ajouter bouton options avec item pour afficher masquer élément avec statue gelé.
      - Déplier replier au clique sur la ligne (adapter au écran tactile)
      - Bouton pour accéder a la vue détaillé.
