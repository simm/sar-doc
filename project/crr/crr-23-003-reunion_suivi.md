
## Réunion de suivi

### 26/01/2023


Prochaine réunion de suivi : 31/01/2023 à 11h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : en cours
  * App : en cours
- Pod :
  - Mise a jour des propriétés manquantes sur le modèle de données (profile hsqldb)
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Recherche
    - Fix [#48](https://gitlab.ifremer.fr/simm/sar-app/-/issues/48)
  * Arbre :
    - Fix [#47](https://gitlab.ifremer.fr/simm/sar-app/-/issues/47)
  * Page :
    - Fix [#43](https://gitlab.ifremer.fr/simm/sar-app/-/issues/43) (help url)
  * Global :
    - Bascule des données du mock au données réellement obtenus depuis le SPARQL endpoint.


> Consommé : [34j au 12/01/23](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- EIS (23/12/2022) :