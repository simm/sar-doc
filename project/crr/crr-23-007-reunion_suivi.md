## Réunion de suivi

### 15/02/2023


Prochaine réunion de suivi : 22/02/2023 à 17h30

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle :

> Consommé : [45,25j au 15/02/23](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning

![Gantt](project/images/planning-suivi.svg)

---

# Actions
- EIS (08/02/2023) :
    - #43 [Typologies Ouvrages] Vue recherche : Recette SAR
      - Rendre la recherche insensible aux accents. C'est la cas pour la première lettre (exemple : épi ==> Epi) mais pas pour les autre lettres (ba ne fait pas ressortir bâ).
      - La colonne 'Statu**s**' doit être renomée en 'Statu**t**'
      - 'COMMON.PAGINATOR.ITEMS_PER_PAGE' apparaît sur navigateur Firefox
      - Le clic sur un élément pour accéder à la landing page doit ouvrir une nouvelle page
    - #44 [Typologies Ouvrages] Vue arbre : Recette SAR
      - Ajouter un bouton pour télécharger le référentiel en entier
      - Le clic sur un élément pour afficher la landing page doit ouvrir une nouvelle fenêtre
    - #45 [Typologies Ouvrages] Landing Page : Recette SAR
      - L'accès aux landing page doit se faire dans une fenêtre différente, ouverture d'un nouvel onglet dans le navigateur
      - Pour les éléments [Type] positionner la [Classe] avant la [Catégorie]
      - Pour les éléments [Classe], il y a une ligne [Classe] or ce devrait être [Catégorie]
      - Afficher le nom du référentiel "Typologie des ouvrages littoraux" (en bleu foncé) pour chaque landing page
      - Le nom de l'élément (label) doit être affiché en bleu clair
      - Bien vérifier le centrage qui ne semble pas être correct entre le titre et les items en dessous
    - #49 [Interlocteurs] Modification du titre
    - #50 [Typologie Ouvrages] Modification du champ 'description' en 'definition'
    - #51 [Typologie Ouvrages] Affichage du titre différent pour la vue arbre et table/recherche

---
# Question