## Réunion de suivi

### 16/03/2023

Prochaine réunion de suivi : 24/03/2023 à 10h

> Présents :
>
> - Glenn JUDEAU (Ifremer)
> - Christian BONNET (Ifremer)
> - Guillaume ALVISET (Data Terra)
> - Benoit LAVENIER (EIS)
> - Maxime DEMAREST (EIS)

---
# Organisation projet

Guillaume Alviset remplace progressivement Glenn Judeau.

La demande d'un export XML (hors RDF) n'est plus d'actualité. Ni le renommage du champ `description` en `definition`. 

---
# RAF / Mise en production

1. Compléter la fiche de mise en production (action Ifremer);
2. Ecrire le fichier de configuration pour le production (action Ifremer);
  * Relecture par EIS
3. Demander la mise en production (action CBO)

---
# Prochains chantiers

1. Finalisation la migration du socle (vers Angular 14, Java 17)
  * Simplifier le fichier de configuration en préproduction (y compris le profile `default`);
2. Nouvelle URL pour accéder aux "landings pages" via les URI
  * Conséquence pour le déploiement : nouvelle URL pour l'APP (en id.milieumarinfrance.fr)

---
# Avancement

> Consommé : [48.5j au 16/03/23](project/sui/sui-22-001-suivi_consomme_eis.md)


---
# Planning

![Gantt](project/images/planning-suivi.svg)t

---
# Actions

---
# Question
 