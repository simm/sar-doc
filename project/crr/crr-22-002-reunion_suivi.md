## Réunion de suivi
### 08/11/2022

> Présents :
>
> - Glenn JUDEAU (IFREMER)
> - Benoit LAVENIER (EIS)
> - Maxime DEMAREST (EIS)

> Absents :
>
> - Antoine HUGUET (IFREMER)


Prochaine réunion de suivi : 18/11/2022 à 11h

---
# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : réalisée
  * App : en cours
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Page de recherche : **UI bien avancé**
  * Page de détail : **Structure de l'UI débutée**

> Consommé : [12j au 04/11/22](project/sui/sui-22-001-suivi_consomme_eis.md)

---
# Planning 

![Gantt](project/images/planning-suivi.svg)

> 28/10/22 - Ifremer indique une baise d'activité à prévoir début 2023 (jusqu'à avril).

---
# Actions

- Glenn (08/11/22) : Répondre aux questions sur la vue arbres
- EIS (date ?) : Livraison recherche CST tableau -  
- EIS (date ?) : Maquettage de la vue arbre (description dépliable, etc.)
- EIS (date ?) : Maquettage page de détails / intégration site