## Réunion de suivi

### 08/02/2023


Prochaine réunion de suivi : 15/02/2023 à 17h30

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Benoît LAVENIER (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle :
    * Pod : en cours
      * Migration liquid base référentiel champ description maintenant limité à 2000 caractères.
      * Correction génération des URI pour les data référentiel.
    * App : en cours
      * Migration Angular 14

> Consommé : [35,5j au 12/01/23](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning

![Gantt](project/images/planning-suivi.svg)

---

# Actions
- EIS (08/02/2023) :
    - Migration pod compatible migration Angular 14
    - Docker avec config nginx nom d'hôte unifié app et pod
    - Aligner sur le titre la "pastille" sur la vue arbre
    - Dans l'app, les chemains ver /data/cst/CoastalStructureType/1 sans paramètre supplémentaires doivent pointer vers la landing page ([#44](https://gitlab.ifremer.fr/simm/sar-app/-/issues/44)).

---

# Question
- EIS (08/02/2023) :
    - Texte d'aide pour les filtres Category est Class