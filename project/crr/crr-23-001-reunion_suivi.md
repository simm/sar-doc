
## Réunion de suivi

### 12/01/2023


Prochaine réunion de suivi : 18/01/2023 à 14h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : réalisée
  * App : en cours
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Recherche
    - Fix [#43](https://gitlab.ifremer.fr/simm/sar-app/-/issues/43)
  * Arbre :
    - Fix [#44](https://gitlab.ifremer.fr/simm/sar-app/-/issues/44)
  * Page :
    - Fix [#45](https://gitlab.ifremer.fr/simm/sar-app/-/issues/45)
  * Divers :
    - Commencé système de notation de page ([#46](https://gitlab.ifremer.fr/simm/sar-app/-/issues/46))


> Consommé : [29j au 12/01/23](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- EIS (23/12/2022) :
  - Mise en place du model de données réel sur le POD.
  - Basculement des données du moc vers les données réels.