## Réunion de suivi

### 15/12/2022


Prochaine réunion de suivi : 23/12/2022 à 13h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement

- Mise à jour du socle : (_inchangé_)
  * Pod : réalisée
  * App : en cours
- Typologie des ouvrages littoraux (CST) : **en cours**
  * Résolution bug hauteur affichage table et vue arbre
  * Exports fonctionnel


> Consommé : [25j au 02/13/22](project/sui/sui-22-001-suivi_consomme_eis.md)

---

# Planning 

![Gantt](project/images/planning-suivi.svg)

---

# Actions

- EIS (23/12/2022) :
  - Typologie des ouvrages littoraux (CST) :
    * Voir les issues sur le Gitlab : <https://gitlab.ifremer.fr/simm/sar-app/-/issues>
    * Produire une version exploitable
    + Vue table
      * Filtres totalement fonctionnels
      * L'export depuis la table doit integer les filtres
      * Uniquement faire figurer le parentID sur l'export
      * Ne pas afficher de badge (status pas sous forme de bouton)
      * Ne pas afficher "pas de parent"
      * Pouvoir rechercher plusieurs mots
    + Vue arbre
      * Ajouter des traits verticaux
      * Afficher le niveau avant le titre