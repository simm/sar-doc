## Réunion de suivi

### 01/03/2023

Prochaine réunion de suivi : 16/03/2023 à 10h00

> Présents :
>
> - Maxime DEMAREST (EIS)
> - Glenn JUDEAU (IFREMER)

---

# Avancement


> Consommé : [47j au 01/03/23](project/sui/sui-22-001-suivi_consomme_eis.md)


# Planning

![Gantt](project/images/planning-suivi.svg)

---

# Actions
- EIS (01/03/2023) :
    - #43 [Typologies Ouvrages] Vue recherche : Recette SAR
      - 'COMMON.PAGINATOR.ITEMS_PER_PAGE' apparaît sur navigateur Firefox

---
# Question
 
- [Typologies Ouvrages] Sous quelle forme présenter les données exporter en JSON et XML ?