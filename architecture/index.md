# Architecture

## Table of contents

- [General architecture](./general_architecture.md) (French)
- Modules serveur :
  * [Core](./core/index_fr.md) (French)
  * [Semantic web](./rdf/index_fr.md) (French)
