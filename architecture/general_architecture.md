# Architecture générale  

![Architecture diagram](architecture/images/general_architecture.png)

Note: Les interfaces d’administration des référentiels opérés par le SAR,
utilisées par une équipe dédiée.

Elles permettent la mise à jour des
instances de référentiels, et la consultation de leur historique. Des
imports de fichiers (ex : shapefile des ports) y sont également
possibles pour des mises à jour plus massives.

---
# Architecture générale

- Une application web (ou App) <!-- .element: class="fragment" -->
- Une application serveur (ou Pod) <!-- .element: class="fragment" -->
- Une base de données <!-- .element: class="fragment" -->
- Un triple store RDF <!-- .element: class="fragment" -->
- Un serveur de traitement <!-- .element: class="fragment" -->

Note: Le système se découpe en 5 composants techniques principaux :
- Une application web (ou App), hébergée sur un serveur
web frontal (sous Docker), permet l’accès aux interfaces de
recherche grand public et l’administration des référentiels
opérés par le SAR. Elle est développée sur le framework
Angular.
- Une application serveur (ou Pod), qui fournit deux points
d’accès d’API distincte : une API web sémantique SparQL (en
lecture seule) pour les interfaces de recherches et les
partenaires ; une API GraphQL pour les référentiels administrés par le
SAR (en lecture/écriture, avec contrôle des droits d’accès).
- Une base de données, permettant la bancarisation des
référentiels opérés par le SAR. Le SGBDR utilisé est PostgreSQL
(10 ou +).
- Un Triple Store RDF, qui stocke des données sémantiques issues : soit
de la base de données ; soit des SI partenaires (Sandre, etc.)
et synchronisées chaque jour via leur propre SparQL endpoint.
Le Triple Store du SAR utilise Apache Jena TDB2.
- Un serveur de traitement, pour exécuter les tâches
régulières ou plus lourdes : Synchronisation depuis les
SparQL externes, import de shapefile, sauvegarde de la base,
etc. A ce jour, le serveur de traitement est intégré et géré dans le Pod,
mais il pourrait être dissocié par la suite, via les options de configuration.

---
## Technologies

- Application **web** (ou App)
  * Angular 12 / Ionic 5 <!-- .element: class="fragment" -->
  * Compatible web et mobile (Progressive Web Application) <!-- .element: class="fragment" --> 

- Application **serveur** (ou Pod)
  * Java / Spring-Boot / Hibernate <!-- .element: class="fragment" -->
  * BDD : PostgreSQL <!-- .element: class="fragment" -->
  * API d'accès aux données : GraphQL <!-- .element: class="fragment" -->
  * Déploiement : Docker <!-- .element: class="fragment" -->
