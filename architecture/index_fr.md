# Architecture 

## Sommaire

- [Architecture générale](./general_architecture.md)
- Modules :
  * [Noyau](./core/index_fr.md)
  * [Web Sémantique](./rdf/index_fr.md)
